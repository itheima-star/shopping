module.exports = {
  publicPath: './',
  devServer: {
    port: 8083,
    open: true,
    // 代理服务器
    proxy: {
      '/api': {
        target: 'http://115.159.87.220:8083'
      },
      '/get': {
        target: 'http://115.159.87.220:8083'
      }
    }
  },
  chainWebpack:config=>{
    // 当前环境为 发布阶段
    config.when(process.env.NODE_ENV == 'production',config=>{
      config.entry('app').clear().add('./src/main-prod.js')
      //使用externals设置排除项
      config.set('externals',{
        vue:'Vue',
        'vue-router':'VueRouter',
        axios:'axios',
        echarts:'echarts',
        nprogress:'NProgress',
        'vue-quill-editor':'VueQuillEditor'
      })
      //使用插件
      config.plugin('html').tap(args=>{
        //添加参数isProd
        args[0].isProd = true
        return args
      })
    })
    // 当前环境为 开发阶段
    config.when(process.env.NODE_ENV == 'development',config=>{
      config.entry('app').clear().add('./src/main-dev.js')
      //使用插件
      config.plugin('html').tap(args=>{
        //添加参数isProd
        args[0].isProd = false
        return args
      })
    })
  }
}