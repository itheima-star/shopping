import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './assets/global.css'
import './assets/fonts/iconfont.css'
import axios from 'axios'
import ZkTable from 'vue-table-with-tree-grid'
import VueQuillEditor from 'vue-quill-editor'
import Nprogress from 'nprogress'

Vue.use(VueQuillEditor)
Vue.component('tree-grid', ZkTable)
Vue.config.productionTip = false
axios.defaults.baseURL = 'http://115.159.87.220:8888/api/private/v1/'
axios.interceptors.request.use(config => {
  // console.log(config)
  config.headers.Authorization = sessionStorage.getItem('token')
  Nprogress.start()
  return config
})
axios.interceptors.response.use(res => {
  Nprogress.done()
  return res
})
Vue.prototype.$axios = axios
Vue.filter('formatTime', (time) => {
  const newDate = new Date(time * 1000)
  const y = newDate.getFullYear()
  const m = (newDate.getMonth() + 1).toString().padStart(2, 0)
  const d = newDate.getDate().toString().padStart(2, 0)
  return y + '-' + m + '-' + d
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
